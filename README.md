# BibliBot

> L'architecture et les arbitrages proposés sont des propositions.

BibliBot est un BOT automatisant une partie de la BIBLIographie scientifique.

**Demande** : *il doit produire en sortie un fichier JSON qui représente un graphe de liaison entre les différents médicaments et leurs mentions respectives dans les différentes publications PubMed, les différentes publications scientifiques et enfin les journaux avec la date associée à chacune de ces mentions. La représentation ci-dessous permet de visualiser ce qui est attendu.*

![plot](./data/data_model.png)

## Architecture
(proposition exploitant GCP)

L'architecture de BibliBot se décompose en 4 éléments : 
- Cloud composer, l'orchestrateur de tâche (airflow)
- Cloud Storage, où sont déposés les fichiers non processés
- BigQuery, où sont uploadées les données processées
- Secret Manager, le gestionnaire de secret
> Pour information aucun de ces services n'est encore utilisé dans le code

### Le modèle de données
Il est probable que BibliBot doivent un jour chercher : 
- dans les publications d'autres types d'élément (pathologie, protéine, etc.)
- dans d'autres types de publication (presse, livre, etc.)
- d'autres types de lien médicament-papier (nombre de mentions, type de mention, mention du principe actif, etc.)

Afin d'anticiper les futurs développements, le projet a été structuré de façon à pouvoir ajouter facilement de nouveaux: 
- élements (pathologie, protéine, etc.) avec la classe `ElementAbstractClass`
- documents (presse, livre, etc.) avec la classe `DocumentAbstractClass`
- liens (type de mention, etc.)

> Les liens ne disposent pas encore de classe abstraite et sont donc moins développés.

### La pipeline

![plot](./data/pipeline_process.jpg)
Le process se déroule en 4 étapes (ces quatre étapes se répètent pour chaque type d'élément)
1. Extraction et transformation des données liées aux médicaments ("éléments")
2. Extraction et transformation des données liées aux publications (pour chaque type de document)
3. Détermination des liens élément-document
4. Sauvegarde des liens

### Fichier output

le fichier est un json contennant la liste des liens (mentions dans notre cas).

Pour chaque mention nous stockons : 
- element_id : L'ID de l'élément (ex : `V03AB` pour l'*ethanol*)
- element_type_id : L'ID interne du type d'élément (ex : `drg` pour *drugs*)
- document_id : L'ID du document (ex: `NCT04188184` pour *Tranexamic Acid Versus Epinephrine During Exploratory Tympanotomy* ) 
- journal : Nom du journal publiant l'article (ex : *Journal of emergency nursing*)
- date : date de la publication au format dd/mm/yyyy (ex : *01/01/2019*)
- document_type_id : L'ID interne du type de document (ex : `mdp` pour *medical publications*)


Ce format présente l'avantage de réduire le nombre de liens stockés (comme une publication n'est associée qu'à un journal)

Pour une base de données relationnelle, il suffirait d'associer une colonne à chaque information (journal, element_id, etc.)

Pour une base de données non relationnelle (Graph ou Document), il faudrait arbitrer entre une de ces deux possibilités : 
- Nous construisons des liens médicament-publication et le journal est un attribut des publications
- Nous construisons des liens médicament-journal et la publication et un attribut du lien

## Choix techniques et arbitrages
(proposition)
### Outils
- Base de données cible (BigQuery vs Graph Database)
- Orchestrateur de tâches (cloud composer / airflow)
- Utilisation d'un outil Big Data (Dataflow, Dataproc)
- Monitoring du process via cloud monitoring & cloud logging

### Détails
- Définir des IDs rigoureux pour chaque type d'entités (médicaments, journal, etc.) afin de garantir la qualité des futures extractions (demande ad-hoc)

### Pistes d'amélioration en cas de saturation
1. Traiter exclusivement le delta-journalier (nouvelles publications)
2. Utiliser un outil Big Data (Dataflow, Dataprox)
3. Paralléliser le traitement (avec spark par exemple). 
   En différenciant le cas où le traitement explose à cause 
   du volume de médicaments ou à cause du volume de publications.

> Il est fondamental de comprendre la source de l'explosion afin d'y apporter la réponse adéquate

## Environments

TODO

## Sources

### `dag_biblibot.py`
Contient le script construisant le DAG (Airflow)
> Code non testé en l'absence de cluster airflow

### `/dependencies_biblibot/pipeline`
Contient : 
- `pipeline.py` : la pipeline de données
- `lib.py` : les différents modules / fonctions de la pipeline
- `/test_file` : les fichiers de données utilisées pour tester la pipeline
- `test_pipeline.py` : le(s) test(s) unitaire(s) de la pipeline

### `/dependencies_biblibot/documents`

Contient : 
- `DocumentAbstractClass.py` : la classe abstraite des Documents
- `<document>CLass.py` : les classes de chaque type de document (publication médicales, essais cliniques, etc.)
- `get_document.py` : des fonctions relatives à la classe documents 

### `/dependencies_biblibot/elements`
- `ElementAbstractClass.py` : la classe abstraite des Eléments
- `<element>CLass.py` : les classes de chaque type d'élément (médicament, pathologie, etc.)
- `get_element.py` : des fonctions relatives à la classe élément 


### `/dependencies_biblibot/links`
Contient l'unique type de lien (actullement) `CitationLinkClass`

### `ad_hoc_request.py`
Contient les différentes demandes ad-hoc. (uniquement `get_journal_with_highest_number_elements.py`pour l'instant) 
> Réponse : *journal of emergency nursing*, *psychopharmacology* et *the journal of maternal fetal neonatal medicine*

Aucun test unitaire n'a été développé car cette fonction sera amené à être modifié, 
une fois le type de base de données défini

### `/sql`
Contient les requête sql `part2.sql` et `part3.sql` associées respectivement à la partie 2 et à la partie 3

## Deploying 

TODO 

## Monitoring

TODO