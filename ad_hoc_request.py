from typing import List

import pandas as pd


def get_journal_with_highest_number_elements(
        filename: str,
) -> List[str]:
    """ Return the list of journal with the highest number of different elements (ex : drug)

    :param filename: location of the link file
    :return: list of journal"""
    # loading list of links
    result = pd.read_json(filename, orient="records")

    # count distinct amount of element mentioned per document
    result = result[["journal", "element_id"]].groupby("journal").nunique()

    # find out the documents with the highest amount of mention
    max_mention = result.element_id.max()
    return list(result.loc[result.element_id == max_mention].index)
