# Exercise
# Réaliser une requête un peu plus complexe qui permet de déterminer, par client et sur la période allant du 1er janvier 2019 au 31 décembre 2019, les ventes meuble et déco réalisées.

SELECT
    client_id,
    SUM(CASE WHEN product_type = "DECO" THEN prod_price*prod_qty ELSE 0 END) AS ventes_deco,
    SUM(CASE WHEN product_type = "MEUBLE" THEN prod_price*prod_qty ELSE 0 END) AS ventes_meuble
FROM TRANSACTION
INNER JOIN PRODUCT_NOMENCLATURE on product_id = prod_id
WHERE date >= "2019-01-01" and date <= "2019-31-01"
GROUP BY client_id
