from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.python_operator import PythonOperator

from dependencies_biblibot import pipeline


# TODO: change start_date en schedule_interval (when deploying)
DEFAULT_ARGS = {
    "owner": "Edouard",
    "depends_on_past": False,
    "start_date": datetime(2021, 1, 1),
    "email": ["edouard.daband@littlebigcode.com"],
    "email_on_retry": False,
    "email_on_failure": False,
    "retries": 1,
    "retry_delay": timedelta(minutes=2)
}
SCHEDULE_INTERVAL = None

ELEMENT_TYPE_ID_LIST = ["drug"]
DOCUMENT_TYPE_ID_LIST = ["medical_publication", "clinical_trial"]
LINK_TYPE_ID = "ctt"


with DAG(
        "biblibot",
        default_args=DEFAULT_ARGS,
        schedule_interval=SCHEDULE_INTERVAL,
) as dag:
    for element_type_id in ELEMENT_TYPE_ID_LIST:
        PythonOperator(
            task_id=f"get_link_for_{element_type_id}",
            python_callable=pipeline,
            op_args=[element_type_id, DOCUMENT_TYPE_ID_LIST, LINK_TYPE_ID],
            dag=dag
        )
