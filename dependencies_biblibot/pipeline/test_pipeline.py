import os
from typing import Dict

import pandas as pd

from .pipeline import pipeline
from ..elements import get_element

LINK_TYPE_ID = "ctt"
ELEMENT_TYPE_LIST = ["drg"]
DOCUMENT_TYPE_LIST = ["mdp", "clt"]


def get_data(element_type_id: str, expected_value: bool) -> pd.DataFrame:
    """ Return the pipeline output or the expected output in order to compare them)

    :param element_type_id: element type id (ex : 'drg')
    :param expected_value: if True return the expected output, if false return the pipeline output
    :return: pipeline output or expected output
    """
    element_type_name: str = get_element(element_type_id).element_type_name
    file_path = os.path.join(
        "dependencies_biblibot",
        "pipeline",
        "test_file",
    )
    if expected_value:
        filename = os.path.join(file_path, f"test_{element_type_name}_processed_data.json")
    else:
        filename = os.path.join(file_path, f"{element_type_name}_processed_data.json")

    return pd.read_json(filename, orient="records")


class TestPipeline:

    @classmethod
    def setup_class(cls) -> None:
        """Run the pipeline for each element type"""
        for element_type_id in ELEMENT_TYPE_LIST:
            pipeline(
                element_type_id=element_type_id,
                document_type_id_list=DOCUMENT_TYPE_LIST,
                link_type_id=LINK_TYPE_ID,
                file_path=os.path.join(
                    "dependencies_biblibot",
                    "pipeline",
                    "test_file"
                )
            )

    @property
    def get_pipeline_output(self) -> Dict:
        """Return a dictionary of all pipeline outputs.

         Dictionary format :
         {
            element_type_id1 : list_of_link1,
            element_type_id2 : list_of_link2,

         }"""
        return {
            element_type_id: get_data(element_type_id, False)
            for element_type_id in ELEMENT_TYPE_LIST
        }

    @property
    def get_expected_value(self) -> Dict:
        """Return the dictionary of all EXPECTED outputs.

         Dictionary format :
         {
            element_type_id1 : list_of_link1,
            element_type_id2 : list_of_link2,

         }"""
        return {
            element_type_id: get_data(element_type_id, True)
            for element_type_id in ELEMENT_TYPE_LIST
        }

    def test_pipeline(self):
        def _func_df_equal(df1: pd.DataFrame, df2: pd.DataFrame) -> bool:
            """Return True if df1 and df2 are equals, return False otherwise."""
            columns_to_sort = ["element_type_id", "document_type_id", "document_id", "element_id"]
            if len(df1) != len(df2):
                return False
            return df1.sort_values(columns_to_sort).reset_index(drop=True).equals(
                df2.sort_values(columns_to_sort)[df1.columns].reset_index(drop=True)
            )

        expected_result = self.get_expected_value
        result_got = self.get_pipeline_output

        # check that all element types have been processed
        assert len(expected_result) == len(result_got)
        # check that all element types have been WELL processed
        for element_type_id in result_got:
            assert _func_df_equal(
                result_got.get(element_type_id),
                expected_result.get(element_type_id)
            )
