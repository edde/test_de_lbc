from typing import Iterable
from logging import Logger
import os

import pandas as pd

from ..elements import ElementAbstractClass
from ..documents import get_document
from ..links import get_link, LinkAbstractClass


def process_doc(
        element_data: pd.DataFrame,
        document_type_id: str,
        link_type_id: str,
        min_word_length: int = 0,
) -> pd.Series:
    """Return list of links concerning a specific element type and a specific document type"""
    document_data: pd.DataFrame = get_document(document_type_id).extract_from_file(min_word_length=min_word_length)
    link: LinkAbstractClass = get_link(link_type_id)
    return pd.concat(
        [
            link.compute_links(element_row, document_data)
            for _, element_row in element_data.iterrows()
         ]
    )


def compute_result(
        element: ElementAbstractClass,
        document_type_id_list: Iterable[str],
        link_type_id: str,
        logger: Logger,
) -> pd.DataFrame:
    """ Return the list of links between a specific element (ex: drugs) and a list of documents.

    :param element: abstract class of an element (ex : drug, pathology, etc.)
    :param document_type_id_list: list of document type id
    :param link_type_id: link type id (ex: "ctt" for citation link)
    :param logger: logger instance
    :return: List of links
    """
    logger.info(f"function `build_json for element {element.element_type_name} started")

    element_data: pd.DataFrame = element.extract_from_file()
    min_word_length: int = element_data.element_name.apply(len).min()

    result = pd.concat(
        [
            process_doc(
                element_data=element_data,
                document_type_id=document_type_id,
                link_type_id=link_type_id,
                min_word_length=min_word_length
            )
            for document_type_id in document_type_id_list
        ]
    )

    logger.info(f"function `build_json for element {element.element_type_name} ended")
    return result


def store_result(
        element: ElementAbstractClass,
        result: pd.DataFrame,
        file_path: str,
        logger,
) -> None:
    """Store all the links of a specific element (ex : drug) in a json file."""
    logger.info(f"function `store_json` for element {element.element_type_name} started")

    filename = f"{element.element_type_name}_processed_data.json"
    result.to_json(os.path.join(file_path, filename), orient="records")

    logger.info(f"function `store_json` for element {element.element_type_name} ended")
