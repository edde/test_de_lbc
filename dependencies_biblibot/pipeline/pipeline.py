from typing import Iterable
import logging
import datetime as dt
import os

import pandas as pd

from .lib import compute_result, store_result
from ..elements import get_element, get_available_element, ElementAbstractClass


# TODO: change location (inputs/outputs/logs)
def pipeline(
        element_type_id: str,
        document_type_id_list: Iterable[str],
        link_type_id: str,
        file_path: str,
) -> None:
    logger_name = "{}_{}_{}_{}_{}_{}_{}".format(
        element_type_id,
        dt.datetime.today().year,
        dt.datetime.today().month,
        dt.datetime.today().day,
        dt.datetime.today().hour,
        dt.datetime.today().minute,
        dt.datetime.today().second,
    )
    logger: logging.Logger = logging.getLogger(logger_name)
    logging.basicConfig(
        level=0,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%a, %d %b %Y %H:%M:%S',
        filename=os.path.join(
            os.getcwd(),
            "dependencies_biblibot",
            "pipeline",
            "test_file"
            '/pipeline_logs.log',
        ),
        filemode='w'
    )
    logger.info(f"pipeline for element {element_type_id} started")
    if element_type_id not in get_available_element():
        logger.error(f"element_name is not an available element")
    else:
        element: ElementAbstractClass = get_element(element_type_id)

        result: pd.DataFrame = compute_result(
            element=element,
            document_type_id_list=document_type_id_list,
            link_type_id=link_type_id,
            logger=logger
        )

        store_result(
            element=element,
            result=result,
            file_path=file_path,
            logger=logger
        )
    logger.info(f"pipeline for element {element_type_id} ended")
