import pandas as pd

from .LinkAbstractClass import LinkAbstractClass


class CitationLinkClass(LinkAbstractClass):
    """Class modeling citation link between documents and elements.
    An element is considered as cited when it is mentioned in the document title."""
    COLUMNS = {
        "document": ["document_id", "date", "journal", "document_type_id"],
        "element": ["element_id", "element_type_id"],
    }

    def doc_to_linked(self, element_row: pd.Series, document_data: pd.DataFrame) -> pd.DataFrame:
        """Select rows (from document_data) to be linked to element_row """
        return document_data.loc[document_data.title.apply(lambda t: element_row.element_name in t)]
