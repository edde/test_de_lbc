from typing import Iterable

from .LinkAbstractClass import LinkAbstractClass
from .CitationLinkClass import CitationLinkClass

DICT_LINK_TYPE = {
    "ctt": CitationLinkClass
}


def get_link(link_type_id: str = "ctt") -> LinkAbstractClass:
    """ Return the abstract class related to an link type id """
    return DICT_LINK_TYPE[link_type_id]()


def get_available_link() -> Iterable:
    """ Return the list of valid link type id """
    return DICT_LINK_TYPE.keys()
