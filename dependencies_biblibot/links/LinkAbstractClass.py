from abc import abstractmethod, ABCMeta
from typing import List

import pandas as pd


class LinkAbstractClass(metaclass=ABCMeta):
    """It is the Abstract class for links between elements and documents

    When creating a child class :

        the following properties and methods have to be overwritten :
            - doc_to_linked

        the following assertions have to be checked :
            - COLUMNS & compute_links store all the information needed
    """

    COLUMNS = {
        "document": ["document_id", "date", "journal", "document_type_id"],
        "element": ["element_id", "element_type_id"],
    }

    @property
    def document_columns(self) -> List[str]:
        """Return the list of document information to be stored"""
        return self.COLUMNS["document"]

    @property
    def element_columns(self) -> List[str]:
        """Return the list of element information to be stored"""
        return self.COLUMNS["element"]

    @abstractmethod
    def doc_to_linked(self, element_row: pd.Series, document_data: pd.DataFrame) -> pd.DataFrame:
        """Select rows (from document_data) to be linked to element_row """
        return document_data.loc[document_data.document_id == element_row.document_id]

    def compute_links(self, element_row: pd.Series, document_data: pd.DataFrame) -> pd.DataFrame:
        """ Return the list of links with element_row

        :param element_row: A specific element (should have at least this indexes : self.COLUMNS["element"]
        :param document_data: list of documents
        :return: list of links
        """
        # Select documents to be linked with element_row
        document_linked: pd.DataFrame = self.doc_to_linked(element_row, document_data)

        # if there is no document
        if len(document_linked) == 0:
            return pd.DataFrame([])

        return document_linked.apply(
            lambda document_row: element_row[self.COLUMNS["element"]].append(document_row[self.COLUMNS["document"]]),
            axis=1
        )
