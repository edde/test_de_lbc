from .ElementAbstractClass import ElementAbstractClass


class DrugClass(ElementAbstractClass):
    """ElementAbstractClass child dedicated to drug."""
    # TODO: change location
    @property
    def file_path(self):
        """Return the file path where the data to be processed is located."""
        return "data/drugs.csv"

    @property
    def element_type_id(self):
        """Return the element type id"""
        return "drg"

    @property
    def element_type_name(self):
        """Return the element type name. It should be more explicit than the id."""
        return "drug"

    @property
    def csv_header(self):
        """Dictionary mapping the csv columns names with the generic names."""
        return {"atccode": "element_id", "drug": "element_name"}
