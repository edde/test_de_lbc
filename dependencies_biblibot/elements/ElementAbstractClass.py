from abc import ABCMeta, abstractmethod

import pandas as pd

from ..util import processing_string


class ElementAbstractClass(metaclass=ABCMeta):
    """It is the Abstract class for elements

    Elements could be drugs, pathologies, proteins, etc.

    When creating a child class :

        the following properties and methods have to be overwritten :
            - file_path
            - element_type_id
            - element_type_name
            - csv_header

        the following assertions have to be checked :
            - no more condition is needed in the method _filter_row
            - the csv delimiter is ","
            - all the date formats are handled by the function : processing_date"""
    @property
    @abstractmethod
    def file_path(self):
        """Return the file path where the data to be processed is located."""
        return ""

    @property
    @abstractmethod
    def element_type_id(self):
        """Return the element type id"""
        return ""

    @property
    @abstractmethod
    def element_type_name(self):
        """Return the element type name. It should be more explicit than the id."""
        return ""

    @property
    @abstractmethod
    def csv_header(self):
        """Dictionary mapping the csv columns names with the generic names

        dictionary format : {
            "csv_name1": "generic_name1",
            "csv_name2": "generic_name2",
            etc.
        }"""
        return {
            "id": "element_id",
            "name": "element_name",
        }

    @property
    def csv_delimiter(self):
        """Return the csv delimiter."""
        return ","

    def extract_from_file(self, file_path: str = None) -> pd.DataFrame:
        """ Extract and standardize data from file

        :param file_path: file path if None take the generic path
        :return: extracted data (element_id, element_name)"""
        if file_path is None:
            file_path = self.file_path

        res = pd.read_csv(file_path, delimiter=self.csv_delimiter)
        res = res.rename(columns=self.csv_header)[self.csv_header.values()]
        res["element_name"] = res["element_name"].apply(processing_string)
        res["element_type_id"] = self.element_type_id

        return res
