from typing import Iterable

from .ElementAbstractClass import ElementAbstractClass
from .DrugClass import DrugClass

DICT_ELEMENT_TYPE = {
    "drg": DrugClass
}


def get_element(element_type_id: str = "drg") -> ElementAbstractClass:
    """ Return the abstract class related to an element type id """
    return DICT_ELEMENT_TYPE[element_type_id]()


def get_available_element() -> Iterable:
    """ Return the list of valid element type id """
    return DICT_ELEMENT_TYPE.keys()
