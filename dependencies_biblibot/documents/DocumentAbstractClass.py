from abc import ABCMeta, abstractmethod
from typing import List, Dict
import re

import pandas as pd

from ..util import processing_date, tockenizing_string, processing_string


class DocumentAbstractClass(metaclass=ABCMeta):
    """ It is the Abstract class for documents

    Documents could be clinical trial, medical publication, etc.

    When creating a child class :

        the following properties and methods have to be overwritten :
            - file_path
            - document_type_id
            - document_type_name
            - csv_header

        the following assertions have to be checked :
            - no more condition is needed in the method _filter_row
            - the csv delimiter is ","
            - all the date formats are handled by the function : processing_date"""
    @property
    @abstractmethod
    def list_file_path(self) -> List[str]:
        """Return the list of file path where the data to be processed is located."""
        return [""]

    @property
    @abstractmethod
    def document_type_id(self) -> str:
        """Return the document type id (ex: 'drg' for drug)"""
        return ""

    @property
    @abstractmethod
    def document_type_name(self) -> str:
        """Return the document type name. It should be more explicit than the id (ex: drug)."""
        return ""

    @property
    @abstractmethod
    def csv_header(self) -> Dict:
        """Dictionary mapping the csv columns names with the generic names

        dictionary format : {
            "csv_name1": "generic_name1",
            "csv_name2": "generic_name2",
            etc.
        }"""
        return {
            "id": "document_id",
            "title": "title",
            "date": "date",
            "journal": "journal"
        }

    @property
    def csv_delimiter(self):
        """Return the csv delimiter."""
        return ","

    @staticmethod
    def _filter_df(data: pd.DataFrame) -> pd.DataFrame:
        """Return the dataframe cleaned (filtered)."""
        return data.dropna()

    def extract_from_file(
            self,
            list_file_path: List[str] = None,
            min_word_length: int = 0
    ) -> pd.DataFrame:
        """ Extract and standardize data from file

        :param list_file_path: list of file path if None take the generic path
        :param min_word_length:
        :return: extracted data (document_id, paper title, date, journal)"""
        if list_file_path is None:
            list_file_path = self.list_file_path
        delimiter = self.csv_delimiter

        def func_aux_extract(filename: str) -> pd.DataFrame:
            if re.match(r".*\.json$", filename):
                return pd.read_json(filename, orient="records")
            else:
                return pd.read_csv(filename, delimiter=delimiter)
        res = pd.concat(
            [
                func_aux_extract(f)
                for f in list_file_path
            ],
        )
        res = res.rename(columns=self.csv_header)[self.csv_header.values()]
        res = self._filter_df(res)
        res["document_id"] = res["document_id"].astype(str)
        res["journal"] = res["journal"].apply(processing_string)
        res["title"] = res["title"].apply(lambda t: tockenizing_string(t, min_word_length))
        res["date"] = res["date"].astype(str).apply(processing_date)
        res["document_type_id"] = self.document_type_id
        return res
