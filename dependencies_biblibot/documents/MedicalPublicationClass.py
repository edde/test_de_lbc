from typing import List, Dict

from .DocumentAbstractClass import DocumentAbstractClass


class MedicalPublicationClass(DocumentAbstractClass):
    """It is the DocumentAbstractClass child dedicated to medical publication."""
    # TODO: change location
    @property
    def list_file_path(self) -> List[str]:
        """Return the list of file path where the data to be processed is located."""
        return ["data/pubmed.csv", "data/pubmed.json"]

    @property
    def document_type_id(self) -> str:
        """Return the document type id (ex: 'drg' for drug)"""
        return "mdp"

    @property
    def document_type_name(self) -> str:
        """Return the document type name. It should be more explicit than the id (ex: drug)."""
        return "medical_publication"

    @property
    def csv_header(self) -> Dict:
        """Dictionary mapping the csv columns names with the generic names"""
        return {
            "id": "document_id",
            "title": "title",
            "date": "date",
            "journal": "journal",
        }
