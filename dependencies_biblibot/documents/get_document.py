from typing import Iterable

from .DocumentAbstractClass import DocumentAbstractClass
from .MedicalPublicationClass import MedicalPublicationClass
from .ClinicalTrialClass import ClinicalTrialClass

DICT_DOCUMENT_TYPE = {
    "mdp": MedicalPublicationClass,
    "clt": ClinicalTrialClass
}


def get_document(document_type_id: str) -> DocumentAbstractClass:
    """ Return the abstract class related to a document type id """
    return DICT_DOCUMENT_TYPE[document_type_id]()


def get_available_document() -> Iterable:
    """ Return the list of valid document type id """
    return DICT_DOCUMENT_TYPE.keys()
