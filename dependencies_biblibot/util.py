from unicodedata import normalize, category
from typing import List
import string


def processing_string(sentence: str) -> str:
    """ Normalize string
    :param sentence: string to normalize
    :return: normalized string
    """
    # remove special character
    res = sentence.replace("\\xc3\\x28", "")

    # lower case
    res = res.lower()

    # remove punctuation
    res = res.translate(str.maketrans(string.punctuation, ' '*len(string.punctuation)))

    # remove accent
    res = ''.join((c for c in normalize('NFD', res) if category(c) != 'Mn'))

    return res


def processing_date(date: str) -> str:
    """ Return the date with the appropriate format : YYYY-MM-DD hh:mm:ss

    :param date: the date
    :return: the date formatted
    """
    if ":" in date:
        return date
    if "/" in date:
        # date is in this format :  dd/mm/yyyy (ex: 01/01/2000)
        dd, mm, yyyy = date.split("/")
        return f"{yyyy}-{mm}-{dd} 00:00:00"
    if "-" in date:
        # date is in this format : yyyy-mm-dd (ex: 2000-01-01)
        return f"{date} 00:00:00"
    else:
        # date is in this format : d month yyyy (ex: 1 january 2000)
        day, month, yyyy = date.split()

        dd = day if len(day) == 2 else f"0{day}"

        month = month.lower()
        if month == "january":
            mm = "01"
        elif month == "february":
            mm = "02"
        elif month == "march":
            mm = "03"
        elif month == "april":
            mm = "04"
        elif month == "may":
            mm = "05"
        elif month == "june":
            mm = "06"
        elif month == "july":
            mm = "07"
        elif month == "august":
            mm = "08"
        elif month == "september":
            mm = "09"
        elif month == "october":
            mm = "10"
        elif month == "november":
            mm = "11"
        else:
            mm = "12"

        return f"{yyyy}-{mm}-{dd} 00:00:00"


def tockenizing_string(sentence: str, min_word_length: int = 0) -> List[str]:
    """ Return the list of words with the shortest words removed

    :param sentence: sentence
    :param min_word_length:  minimum word length (if min_word_length = 4 the word "car" would be removed
    :return: list of words
    """
    return [w for w in processing_string(sentence).split() if len(w) >= min_word_length]
